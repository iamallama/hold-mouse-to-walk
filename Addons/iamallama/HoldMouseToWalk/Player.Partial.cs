﻿using UnityEngine;

public partial class Player : Entity {

	//change this if you need to use different timing
	private float secondsBetweenMovementRequests = 0.5f;

	//shouldn't have to change anything below this
	private bool isMouseDown = false;
	private float nextMovement = 0f;

	public void UpdateClient_HoldMouseToWalk() {
		//if mouse up, reset everything
		if (Input.GetMouseButtonUp(0)) {
			//reset
			isMouseDown = false;
		}

		if (isLocalPlayer) {
			if (state == "IDLE" || state == "MOVING" || state == "CASTING") {
				//if mouse is down
				if (isMouseDown || (Input.GetMouseButtonDown(0) && !Utils.IsCursorOverUserInterface() && Input.touchCount <= 1)) {
#if IAMALLAMA_HOLD_MOUSE_TO_WALK_2D
					Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
					// Detect hit
					RaycastHit2D hit = Utils.Raycast2DWithout(ray, gameObject);

					Entity entity = hit.transform != null ? hit.transform.GetComponent<Entity>() : null;

					//if mouse is held down
					if (isMouseDown) {
						//subtract the delta time
						nextMovement -= Time.deltaTime;
						//if it is time to move, initiate move and reset timer
						if (nextMovement <= 0f) {
							nextMovement = secondsBetweenMovementRequests;
							Vector2 worldPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);

							// set indicator and navigate to the nearest walkable
							// destination. this prevents twitching when destination is
							// accidentally in a room without a door etc.
							var bestDestination = agent.NearestValidDestination(worldPos);
							SetIndicatorViaPosition(bestDestination);
							CmdNavigateDestination(bestDestination, 0);
						}
					} else if (!entity) {
						//it's a movement target
						//set mouseDown to true
						isMouseDown = true;
						//set the timer for next movement
						nextMovement = secondsBetweenMovementRequests;
					}
#elif IAMALLAMA_HOLD_MOUSE_TO_WALK_3D // else if 3d, we need to check with a 3d raycast hit
					Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
					RaycastHit hit;

					//if hit something
					if (Physics.Raycast(ray, out hit)) {
						Entity entity = hit.transform != null ? hit.transform.GetComponent<Entity>() : null;

						//if mouse is held down
						if (isMouseDown) {
							//subtract the delta time
							nextMovement -= Time.deltaTime;
							//if it is time to move, initiate move and reset timer
							if (nextMovement <= 0f) {
								nextMovement = secondsBetweenMovementRequests;
								// set indicator and navigate to the nearest walkable
								// destination. this prevents twitching when destination is
								// accidentally in a room without a door etc.
								var bestDestination = agent.NearestValidDestination(hit.point);
								SetIndicatorViaPosition(bestDestination);
								CmdNavigateDestination(bestDestination, 0);
							}
						} else if (!entity) {
							//it's a movement target
							//set mouseDown to true
							isMouseDown = true;
							//set the timer for next movement
							nextMovement = secondsBetweenMovementRequests;
						}
					}
#endif
				}
			}
		}
	}
}
