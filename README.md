## HoldMouseToWalk V1.1

Simple add-on to allow you to hold the mouse down to keep walking towards the mouse.

---

## Contents

1. Installation
2. Contact
3. Changes

## Installation

Just import the package and it's ready to go. By default, this will issue a new navigation request once every 0.5 seconds. You can change this in code by changing the secondsBetweenMovementRequests variable.

## Contact

The easiest way to contact me would be to send me a DM on discord to @iamallama#1857. I am on quite a lot and am willing to offer support and community help in any way that I can.

## Changes

v 1.1: Updated to work with latest version of uMMORPG (v1.30 in 2d and v1.137 in 3d). Updated to work in both 3d and 2d versions of uMMORPG.
v 1.0: Initial upload.